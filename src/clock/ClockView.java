class ClockView extends javax.swing.JFrame {

    private javax.swing.JLabel tLabel = new javax.swing.JLabel();

    ClockView() {   // constructor
        this.setDefaultCloseOperation(
              javax.swing.WindowConstants.EXIT_ON_CLOSE);
        this.setSize(95,45);
        this.getContentPane().add(tLabel);
        this.refreshTimeDisplay();
    }

    protected String getDigitsAsString(int i) {
        //String str = Integer.toString(i);
        //if (i<10) str = "0"+str;
        return getRomanNumerals(i);
    }

    public void refreshTimeDisplay() {
        Timestamp t = new Timestamp();
        t.fillTimes();
        String display = getDigitsAsString(t.hrs) + ":"
                     + getDigitsAsString(t.mins)  + ":"
                     + getDigitsAsString(t.secs);
        tLabel.setText("  " + display );
        tLabel.repaint();
    }
    
    private String getRomanNumerals(int i) {
        String roman = "";
        
        while(i >= 50)
        {
            roman += "L";
            i -= 50;
        }
        
        while(i >= 40)
        {
            roman += "XL";
            i -= 40;
        }
        
        while(i >= 10)
        {
            roman += "X";
            i -= 10;
        }
        
        while(i >= 9)
        {
            roman += "IX";
            i -= 9;
        }
        
        while(i >= 5)
        {
            roman += "V";
            i -= 5;
        }
        
        while(i >= 4)
        {
            roman += "IV";
            i -= 4;
        }
        
        while(i >= 1)
        {
            roman += "I";
            i -= 1;
        }
        
        return roman;
    }

}
