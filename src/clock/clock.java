public class clock {

    public static void main(String args[]) {
        ClockView cv = new ClockView();
        cv.setVisible(true);
        // loop about every 0.5 seconds
        try {
            for (;;) {
              cv.refreshTimeDisplay();
              Thread.sleep(500);
            }
        } catch (Exception e) {System.out.println("Error:"+e) ; }
    }

}
